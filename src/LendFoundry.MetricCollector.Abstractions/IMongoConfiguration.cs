﻿namespace LendFoundry.MetricCollector
{
    public interface IMongoConfiguration
    {
        string ConnectionString { get; set; }
        string Database { get; set; }
    }
}
