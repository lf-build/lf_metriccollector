﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.MetricCollector
{
    public interface IApplicationFact : IAggregate
    {
        string ApplicationNumber { get; set; }
        TimeBucket AppliedDateTime { get; set; }
        string ApplicationSource { get; set; }
        string SubmittedByAgent { get; set; }
        string ApplicationStatus { get; set; }
        string HomeType { get; set; }
        //double RequestedTerm { get; set; }//remove
        //double RequestedAmount { get; set; }
        string LoanPurpose { get; set; }
        string State { get; set; }
        string City { get; set; }
        string Zipcode { get; set; }
        //string CreditScore { get; set; }
       // string CreditGrade { get; set; }
        string AnnualIncome { get; set; }

        string PartnerId { get; set; }
    }
}
