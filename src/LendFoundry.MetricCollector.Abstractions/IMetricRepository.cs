﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.MetricCollector;

namespace LendFoundry.MetricCollector
{
    public interface IMetricRepository : IRepository<IMetric>
    {
        void AddOrUpdate(IMetric fact);
        IMetric GetExistingMetric(string tenantId, string metricName);
    }
}