﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.MetricCollector
{
    public class RuleResponse
    {
        public CaseInsensitiveDictionary<object> facts { get; set; }
        public CaseInsensitiveDictionary<object> dimensions { get; set; }
        public TimeBucket ApplicationDate { get; set; }
    }

}
