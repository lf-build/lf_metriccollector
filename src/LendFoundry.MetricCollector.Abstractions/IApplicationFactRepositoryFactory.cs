﻿using LendFoundry.MetricCollector;
using LendFoundry.Security.Tokens;

namespace LendFoundry.MetricCollector
{
    public interface IApplicationFactRepositoryFactory
    {
        IApplicationFactRepository Create(ITokenReader reader);
    }
}