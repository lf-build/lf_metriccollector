﻿namespace LendFoundry.MetricCollector
{
    public class MongoConfiguration : IMongoConfiguration
    {
        public MongoConfiguration(string connectionString, string database)
        {
            ConnectionString = connectionString;
            Database = database;
        }

        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
