﻿namespace LendFoundry.MetricCollector
{
    public enum OperationType
    {
        Increament,
        Sum
    }
}
