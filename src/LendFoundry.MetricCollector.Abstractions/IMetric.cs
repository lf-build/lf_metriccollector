﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.MetricCollector
{
    public interface IMetric : IAggregate
    {
        string MetricName { get; set; }
         List<Dimention> Dimentions { get; set; }
         List<Fact> Facts { get; set; }
        TimeBucket ApplicationDate { get; set; }
    }
}