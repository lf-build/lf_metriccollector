﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.MetricCollector
{
    public interface IMetricRepositoryFactory
    {
        IMetricRepository Create(ITokenReader reader);
    }
}