﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.MetricCollector
{
    public class ApplicationFact : Aggregate {
        public string ApplicationNumber { get; set; }
        public TimeBucket AppliedDateTime { get; set; }
        public string ApplicationSource { get; set; }
        public string SubmittedByAgent { get; set; }
        public string ApplicationStatus { get; set; }
        public string HomeType { get; set; }
        public double RequestedTerm { get; set; }
        public double RequestedAmount { get; set; }
        public string LoanPurpose { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string CreditScore { get; set; }
        public string CreditGrade { get; set; }
        public string AnnualIncome { get; set; }
    }
}
