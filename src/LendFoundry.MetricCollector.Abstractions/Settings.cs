﻿using System;

namespace LendFoundry.MetricCollector
{
    public static class Settings
    {
         public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "metric-collector";
    }
}
