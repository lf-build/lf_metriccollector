﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.MetricCollector
{
    public interface IApplicationFactRepository : IRepository<IApplicationFact>
    {
        void AddOrUpdate(IApplicationFact fact);
    }
}
