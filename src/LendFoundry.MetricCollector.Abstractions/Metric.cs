﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.MetricCollector
{
    public class Metric : Aggregate, IMetric
    {
        public string MetricName { get; set; }
        public List<Dimention> Dimentions { get; set; }
        public List<Fact> Facts { get; set; }
        public TimeBucket ApplicationDate { get; set; }
    }

    public class Dimention
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
    public class Fact
    {
        public string Name { get; set; }
        public double Value { get; set; }

    }
}
