﻿using System.Collections.Generic;

namespace LendFoundry.MetricCollector.Configurations
{
    public class MetricRule
    {
        public string EntityType { get; set; }
        public string ApplicationNumber { get; set; }
        public string TenantId { get; set; }
        public string OriginalEventName { get; set; }
        public Dictionary<string, EventMetric> EventMetric { get; set; } 
        public string ProductId { get; set; }
    }

    public class EventMetric
    {
        public string RuleName { get; set; }

        public List<string> DataSources { get; set; }
        public List<ConfigurationMetric> Metrics { get; set; }
    }

    public class ConfigurationMetric
    {
        public string RuleName { get; set; }
        public string MetricName { get; set; }
        public List<string> Dimensions { get; set; }
        public List<Fact> Facts { get; set; }
    }

    public class Fact
    {
        public string Name { get; set; }
        public string Operation { get; set; }
    }
}
