﻿using System.Collections.Generic;

namespace LendFoundry.MetricCollector.Configurations
{
    public interface IMetricConfiguration
    {
        List<MetricRule> MetricRules { get; set; }
    }
}
