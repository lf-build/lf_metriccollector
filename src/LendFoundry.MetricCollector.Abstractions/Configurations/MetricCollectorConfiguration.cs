﻿using LendFoundry.Foundation.Client;
using LendFoundry.MetricCollector;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.MetricCollector.Configurations
{
    public class MetricCollectorConfiguration : IDependencyConfiguration
    {
        public Dictionary<string, MetricRule> Metrics { get; set; }

        public List<string> NonActiveStatuses { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IMongoConfiguration, MongoConfiguration>))]
        public IMongoConfiguration MongoConfiguration { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}
