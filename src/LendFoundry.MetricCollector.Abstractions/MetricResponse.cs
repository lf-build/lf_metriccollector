﻿using System.Collections.Generic;

namespace LendFoundry.MetricCollector
{
    public class MetricResponse
    {
        public int TotalCount { get; set; }
        public List<MetricCollection> MetricData { get; set; }
    }
    public class MetricCollection
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
