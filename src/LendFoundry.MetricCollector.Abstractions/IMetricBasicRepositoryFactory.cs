﻿using LendFoundry.MetricCollector.Configurations;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.MetricCollector
{
    public interface IMetricBasicRepositoryFactory
    {
        IMetricBasicRepository Create(ITokenReader reader, MetricCollectorConfiguration configuration);
    }
}
