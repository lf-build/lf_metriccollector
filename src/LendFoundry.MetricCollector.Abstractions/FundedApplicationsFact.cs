﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.MetricCollector
{
    public class FundedApplicationsFact : Aggregate
    {
        public string ApplicationNumber { get; set; }
        public TimeBucket FundingDateTime { get; set; }
        public string FundedBy { get; set; }
        public string LoanTerm { get; set; }
        public string LoanAmount { get; set; }
        public double ProcessingFees { get; set; }
        public double InterestRate { get; set; }
        public string APR { get; set; }
        public string DTI { get; set; }
        public double CommisionPaid { get; set; }
        public string Investor { get; set; }
        public double PrincipalBalance { get; set; }
        public double InterestBalance { get; set; }
        public double LateCharges { get; set; }
        public TimeBucket PayoffTillDate { get; set; }
        public double PaymentsRemaining { get; set; }
        public double PaymentsMade { get; set; }
        public TimeBucket RemainingAmount { get; set; }
    }
}
