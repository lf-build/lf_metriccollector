﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.MetricCollector
{
    public class CaseInsensitiveDictionary<TValue> : Dictionary<string, TValue>
    {
        public CaseInsensitiveDictionary() : base(StringComparer.OrdinalIgnoreCase)
        {
        }

        public CaseInsensitiveDictionary(CaseInsensitiveDictionary<TValue> values) : base(values, StringComparer.OrdinalIgnoreCase)
        {
        }

        public CaseInsensitiveDictionary(Dictionary<string, TValue> values) : base(values, StringComparer.OrdinalIgnoreCase)
        {
        }
    }
}
