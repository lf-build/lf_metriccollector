﻿using MongoDB.Bson;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.MetricCollector
{
    public interface IMetricCollectorService
    {
        Task<MetricResponse> GetAllApplicationCountByStatus(int timeSpan);
        //string GetData(string collectionName, string fact, string groupKey, string operatorData);
        Task<List<PartnerIdMetricResponse>> GetPartnerApplicationData(string partnerId, int timeSpan);
        Task<List<MetricCollection>> GetApplicationRejectReasonData();
        JArray GetData(string collectionName, string fact, string groupKey, string operatorData);
        Task<MetricResponse> GetAllPartnerApplicationCountByStatus(int timeSpan, string partnerId = null);
    }
}