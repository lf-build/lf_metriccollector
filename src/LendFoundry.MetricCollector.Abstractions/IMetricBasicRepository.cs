﻿using LendFoundry.Security.Tokens;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.MetricCollector
{
    public interface IMetricBasicRepository
    {
        IMongoDatabase GetDatabase();
        JArray GetData(string collectionName, string fact, string groupKey, string operatorData);
        Task<List<PartnerIdMetricResponse>> GetPartnerIdData(string partnerId, DateTimeOffset startDate, DateTimeOffset endDate);
        Task<List<PartnerIdMetricResponse>> GetAllPartnerIdData(string partnerId);
        Task<List<BsonDocument>> GetRejectReasonData();
    }
}
