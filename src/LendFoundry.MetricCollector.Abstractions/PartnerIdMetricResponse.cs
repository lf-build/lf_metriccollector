﻿namespace LendFoundry.MetricCollector
{
    public class PartnerIdMetricResponse
    {
        public string TenantId { get; set; }
        public string PartnerId { get; set; }
        public string FundingDate { get; set; }
        public double FundedAmount { get; set; }
        public double CommissionAmount { get; set; }
        public double OriginatingFeeAmount { get; set; }
        public double ApprovedAmount { get; set; }
        public double FundedApplicationCount { get; set; }
    }
}
