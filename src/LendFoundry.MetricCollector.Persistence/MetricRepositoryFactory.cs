﻿using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.MetricCollector;
using LendFoundry.MetricCollector.Persistence;
using LendFoundry.MetricCollector.Configurations;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.MetricCollector.Persistence
{
    public class MetricRepositoryFactory : IMetricBasicRepositoryFactory
    { 
        public MetricRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }
        public IMetricBasicRepository Create(ITokenReader reader, MetricCollectorConfiguration configuration)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
          
            return new MetricBasicRepository(tenantService, configuration.MongoConfiguration);
        }
    }
}
