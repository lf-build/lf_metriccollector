﻿using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using System;
using MongoDB.Bson;
using LendFoundry.Security.Tokens;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LendFoundry.MetricCollector.Persistence
{
    public class MetricBasicRepository : IMetricBasicRepository
    {
        public MetricBasicRepository(ITenantService tenantService, IMongoConfiguration configuration)
        {
            var client = new MongoClient(configuration.ConnectionString);
            Database = client.GetDatabase(configuration.Database);
            TenantService = tenantService;
            Configuration = configuration;

        }

        protected IMongoDatabase Database { get; }
        protected ITenantService TenantService { get; }

        public IMetricBasicRepository Create(ITokenReader reader)
        {
            throw new NotImplementedException();
        }

        public IMongoDatabase GetDatabase()
        {
            return Database;
        }
        private IMongoConfiguration Configuration { get; }

        public async Task<List<PartnerIdMetricResponse>> GetPartnerIdData(string partnerId, DateTimeOffset startDate, DateTimeOffset endDate)
        {
            List<BsonDocument> partnerIdData = null;

            var tenantIdMatch = new BsonDocument { { "$match", new BsonDocument { { "TenantId", TenantService.Current.Id } } } };
            var partnerIdMatch = new BsonDocument { { "$match", new BsonDocument { { "PartnerId", partnerId } } } };

            List<BsonDocument> matchCritiria = new List<BsonDocument>();

            matchCritiria.Add(tenantIdMatch);
            matchCritiria.Add(partnerIdMatch);

            List<PartnerIdMetricResponse> result = new List<PartnerIdMetricResponse>();
            var data = Database.GetCollection<BsonDocument>("FundingPartnerIdMetric");

            if (data != null)
            {
                partnerIdData = await data.Aggregate<BsonDocument>(matchCritiria).ToListAsync();
            }
            foreach (var row in partnerIdData)
            {
                PartnerIdMetricResponse metricResponse = new PartnerIdMetricResponse();
                metricResponse.TenantId = row.Elements.Where(x => x.Name == "TenantId").FirstOrDefault().Value.ToString();
                if (row.Elements.Where(x => x.Name == "PartnerId")?.FirstOrDefault() != null)
                    metricResponse.PartnerId = row.Elements.Where(x => x.Name == "PartnerId").FirstOrDefault().Value.ToString();
                if (row.Elements.Where(x => x.Name == "FundingDate")?.FirstOrDefault() != null)
                    metricResponse.FundingDate = row.Elements.Where(x => x.Name == "FundingDate").FirstOrDefault().Value.ToString();
                if (row.Elements.Where(x => x.Name == "CommissionAmount")?.FirstOrDefault() != null)
                    metricResponse.CommissionAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "CommissionAmount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "FundedAmount")?.FirstOrDefault() != null)
                    metricResponse.FundedAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "FundedAmount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "ApprovedAmount")?.FirstOrDefault() != null)
                    metricResponse.ApprovedAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "ApprovedAmount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "FundedApplicationCount")?.FirstOrDefault() != null)
                    metricResponse.FundedApplicationCount = Convert.ToDouble(row.Elements.Where(x => x.Name == "FundedApplicationCount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "OriginatingFeeAmount")?.FirstOrDefault() != null)
                    metricResponse.OriginatingFeeAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "OriginatingFeeAmount").FirstOrDefault().Value);
                result.Add(metricResponse);
            }
            result = result.Where(x => Convert.ToDateTime(x.FundingDate) >= startDate && Convert.ToDateTime(x.FundingDate) <= endDate).ToList();

            return result;
        }

        public async Task<List<PartnerIdMetricResponse>> GetAllPartnerIdData(string partnerId)
        {
       
            List<BsonDocument> partnerIdData = null;
            List<PartnerIdMetricResponse> result = new List<PartnerIdMetricResponse>();
            var filters = new List<FilterDefinition<BsonDocument>>();
            filters.Add(Builders<BsonDocument>.Filter.Eq("TenantId", TenantService.Current.Id));
            filters.Add(Builders<BsonDocument>.Filter.Eq("PartnerId", partnerId));
            var data = Database.GetCollection<BsonDocument>("FundingPartnerIdMetric");
            var finalFilter = Builders<BsonDocument>.Filter.And(filters);
            if (data != null)
            {
                partnerIdData =data.Find(finalFilter).ToList();
            }
            await Task.Run (() => {
            foreach (var row in partnerIdData)
            {
                PartnerIdMetricResponse metricResponse = new PartnerIdMetricResponse();
                metricResponse.TenantId = row.Elements.Where(x => x.Name == "TenantId").FirstOrDefault().Value?.ToString();
                if (row.Elements.Where(x => x.Name == "PartnerId")?.FirstOrDefault() != null)
                    metricResponse.PartnerId = row.Elements.Where(x => x.Name == "PartnerId").FirstOrDefault().Value?.ToString();
                if (row.Elements.Where(x => x.Name == "FundingDate")?.FirstOrDefault() != null)
                    metricResponse.FundingDate = row.Elements.Where(x => x.Name == "FundingDate").FirstOrDefault().Value?.ToString();
                if (row.Elements.Where(x => x.Name == "CommissionAmount")?.FirstOrDefault() != null)
                    metricResponse.CommissionAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "CommissionAmount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "FundedAmount")?.FirstOrDefault() != null)
                    metricResponse.FundedAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "FundedAmount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "ApprovedAmount")?.FirstOrDefault() != null)
                    metricResponse.ApprovedAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "ApprovedAmount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "FundedApplicationCount")?.FirstOrDefault() != null)
                    metricResponse.FundedApplicationCount = Convert.ToDouble(row.Elements.Where(x => x.Name == "FundedApplicationCount").FirstOrDefault().Value);
                if (row.Elements.Where(x => x.Name == "OriginatingFeeAmount")?.FirstOrDefault() != null)
                    metricResponse.OriginatingFeeAmount = Convert.ToDouble(row.Elements.Where(x => x.Name == "OriginatingFeeAmount").FirstOrDefault().Value);
                result.Add(metricResponse);
            }
            });
            return result;
        }
        public async Task<List<BsonDocument>> GetRejectReasonData()
        {
            List<BsonDocument> rejectReasonData = null;

            var tenantIdMatch = new BsonDocument { { "$match", new BsonDocument { { "TenantId", TenantService.Current.Id } } } };

            List<BsonDocument> matchCritiria = new List<BsonDocument>();
            matchCritiria.Add(tenantIdMatch);

            var data = Database.GetCollection<BsonDocument>("ApplicationRejctionMetric");

            if (data != null)
            {
                rejectReasonData = await data.Aggregate<BsonDocument>(matchCritiria).ToListAsync();
            }
            return rejectReasonData;
        }

        public JArray GetData(string collectionName, string fact, string groupKey, string operatorData)
        {

            var command = new JsonCommand<BsonDocument>("{return db.getCollection('FundingZipCodeMetric').aggregate([{$group:{ _id: '$FundingDate','FundedAmount': { $sum: '$FundedAmount' },'CommissionAmount': { $sum: '$CommissionAmount' } }},{$project:{ _id: 0 ,State: '$_id',TotalFundedAmount: '$FundedAmount',TotalCommissionAmount: '$CommissionAmount'}}])}");
            var test = Database.RunCommand<BsonDocument>(command);

            return new JArray();
        }

        private static JArray GetResultValue(dynamic data, string variableName)
        {
            JArray result = null;
            if (data != null)
            {
                var myObj = JsonConvert.DeserializeObject<dynamic>(data.ToString());
                result = myObj[variableName];
            }
            return result;
        }
    }
}
