﻿using LendFoundry.Applications.Filters.Abstractions.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Applications.Filters.Abstractions;
using MongoDB.Bson;
using System;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json.Linq;

namespace LendFoundry.MetricCollector
{
    /// <summary>
    ///  MetricCollectorService class
    /// </summary>
    public class MetricCollectorService : IMetricCollectorService
    {
        /// <summary>
        ///  constructor
        /// </summary>
        /// <param name="applicationFilterService"></param>
        /// <param name="repository"></param>
        /// <param name="tenantTime"></param>
        public MetricCollectorService(
          IApplicationFilterService applicationFilterService, IMetricBasicRepository repository, ITenantTime tenantTime)
        {
            ApplicationFilterService = applicationFilterService;
            Repository = repository;
            TenantTime = tenantTime;
        }
        private IApplicationFilterService ApplicationFilterService { get; }

        private IMetricBasicRepository Repository { get; }

        private ITenantTime TenantTime { get; }

        /// <summary>
        /// GetAllApplicationCountByStatus
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <returns></returns>
        public async Task<MetricResponse> GetAllApplicationCountByStatus(int timeSpan)
        {
            MetricResponse metricDetail = new MetricResponse();
            IEnumerable<IApplicationStatusGroupView> metricCollection = null;
            if(timeSpan > 0)
            {
                metricCollection = await ApplicationFilterService.GetApplicationCountByStatus(timeSpan);
            }
            else
            {
                metricCollection = await ApplicationFilterService.GetApplicationCountByStatus();
            }
            if(metricCollection != null)
            {
                metricDetail.MetricData = metricCollection.Select(x => new MetricCollection() { Name = x.Status, Count = x.Count }).ToList();
                metricDetail.TotalCount = metricDetail.MetricData.Sum(x => x.Count);
            }
            return metricDetail;
        }

        /// <summary>
        /// GetAllPartnerApplicationCountByStatus
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public async Task<MetricResponse> GetAllPartnerApplicationCountByStatus(int timeSpan,string partnerId = null)
        {
            MetricResponse metricDetail = new MetricResponse();
            IEnumerable<IApplicationStatusGroupView> metricCollection = null;
            if (timeSpan > 0)
            {
                metricCollection = await ApplicationFilterService.GetPartnerApplicationCountByStatus(timeSpan, partnerId);
            }
            else
            {
                metricCollection = await ApplicationFilterService.GetPartnerApplicationCountByStatus(partnerId);
            }
            if (metricCollection != null)
            {
                metricDetail.MetricData = metricCollection.Select(x => new MetricCollection() { Name = x.Status, Count = x.Count }).ToList();
                metricDetail.TotalCount = metricDetail.MetricData.Sum(x => x.Count);
            }
            return metricDetail;
        }

        /// <summary>
        /// GetPartnerApplicationData
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="timeSpan"></param>
        /// <returns></returns>
        public async Task<List<PartnerIdMetricResponse>> GetPartnerApplicationData(string partnerId, int timeSpan)
        {
            List<PartnerIdMetricResponse> metricDetail = null;
            List<BsonDocument> matchCritiria = new List<BsonDocument>();

            if(timeSpan > 0)
            {
                DateTimeOffset fromDate = TenantTime.Today.AddDays(-timeSpan);
                DateTimeOffset toDate = TenantTime.Today;

                metricDetail = await Repository.GetPartnerIdData(partnerId, fromDate, toDate);
            }
            else
            {
                metricDetail = await Repository.GetAllPartnerIdData(partnerId);
            }
            return metricDetail;
        }

        /// <summary>
        /// GetApplicationRejectReasonData
        /// </summary>
        /// <returns></returns>
        public async Task<List<MetricCollection>> GetApplicationRejectReasonData()
        {
            List<MetricCollection> rejectReasonList = null;
            var rejectReasonData =  await Repository.GetRejectReasonData();
            if(rejectReasonData != null && rejectReasonData.Count > 0)
            {
                rejectReasonList = new List<MetricCollection>();
                foreach (var reason in rejectReasonData)
                {
                    MetricCollection collection = new MetricCollection();
                    collection.Name = Convert.ToString(reason.Elements.Where(x => x.Name == "DeclineReason").FirstOrDefault().Value);
                    collection.Count = (int)Convert.ToDouble(reason.Elements.Where(x => x.Name == "ApplicationCount").FirstOrDefault().Value);
                    rejectReasonList.Add(collection);
                }
            }
            return rejectReasonList;
        }

        /// <summary>
        /// GetData
        /// </summary>
        /// <param name="collectionName"></param>
        /// <param name="fact"></param>
        /// <param name="groupKey"></param>
        /// <param name="operatorData"></param>
        /// <returns></returns>
        public JArray GetData(string collectionName, string fact, string groupKey, string operatorData)
        {
            var result = Repository.GetData(collectionName, fact, groupKey, operatorData);
            return result;
        }
    }
}
