﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Linq;
using System.Net.WebSockets;
using LendFoundry.MetricCollector.Configurations;
using LendFoundry.StatusManagement.Client;
using LendFoundry.StatusManagement;
using LendFoundry.ProductRule.Client;
using LendFoundry.ProductRule;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using LendFoundry.DataAttributes.Client;

namespace LendFoundry.MetricCollector
{
    /// <summary>
    /// MetricCollectorListner class
    /// </summary>
    public class MetricCollectorListner : IMetricCollectorListner
    {
        /// <summary>
        ///  MetricCollectorListner
        /// </summary>
        /// <param name="tokenHandler"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="eventHubFactory"></param>
        /// <param name="tenantServiceFactory"></param>
        /// <param name="tenantTimeFactory"></param>
        /// <param name="configurationFactory"></param>
        /// <param name="apiConfigurationFactory"></param>
        /// <param name="statusServiceFactory"></param>
        /// <param name="productRuleServiceFactory"></param>
        /// <param name="metricRepositoryFactory"></param>
        public MetricCollectorListner
        (
            ITokenHandler tokenHandler,
            ILoggerFactory loggerFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory,
            IConfigurationServiceFactory<MetricCollectorConfiguration> apiConfigurationFactory,
            IStatusManagementServiceFactory statusServiceFactory,
            IProductRuleServiceClientFactory productRuleServiceFactory,
            IMetricBasicRepositoryFactory metricRepositoryFactory
        )
        {
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            EventHubFactory = eventHubFactory;
            TenantServiceFactory = tenantServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
            StatusServiceFactory = statusServiceFactory;
            ConfigurationMetricCollector = apiConfigurationFactory;
            ProductRuleServiceFactory = productRuleServiceFactory;
            MetricRepositoryFactory = metricRepositoryFactory;
        }

        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        private IConfigurationServiceFactory<MetricCollectorConfiguration> ConfigurationMetricCollector { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IDecisionEngineClientFactory DecisionEngineFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IStatusManagementServiceFactory StatusServiceFactory { get; }
        private IProductRuleServiceClientFactory ProductRuleServiceFactory { get; }
        private IMetricBasicRepositoryFactory MetricRepositoryFactory { get; }
        private IDataAttributesClientFactory DataAttributesFactory { get; }
        
        /// <summary>
        /// Start
        /// </summary>
        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);

                tenantService.GetActiveTenants().ForEach(tenant =>
                {
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var configuration = ConfigurationMetricCollector.Create(reader).Get();
                    if (configuration == null)
                    {
                        logger.Info($"Configuration not found for tenant #{tenant.Id}");
                        return;
                    }
                    if (configuration.Metrics == null)
                    {
                        logger.Info($"No Metrics defined for tenant #{tenant.Id}");
                        return;
                    }
                    var eventHub = EventHubFactory.Create(reader);
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var statusService = StatusServiceFactory.Create(reader);
                    var productRuleService = ProductRuleServiceFactory.Create(reader);
                    var repository = MetricRepositoryFactory.Create(reader, configuration);
                    configuration
                       .Metrics
                       .ToList()
                       .ForEach(eventConfig =>
                        {
                           eventHub.On(eventConfig.Key, AddMetric(eventConfig.Value, logger, statusService, tenantTime, productRuleService, repository));
                           logger.Info($"Event #{eventConfig.Key} attached to listener");
                        });
                    eventHub.StartAsync();
                });
                logger.Info("Listening to events");
            }
            catch (WebSocketException ex)
            {
                logger.Error("Connection error listening to events", ex);
                // Start();
            }
        }

        private static Action<EventInfo> AddMetric
       (
            MetricRule metricConfiguration,
            ILogger logger,
            IEntityStatusService statusManagementService,
            ITenantTime tenantTime,
            IProductRuleService productRuleService,
            IMetricBasicRepository metricRepository
        )
        {
            return async @event =>
            {
                try
                {
                    //foreach (var metric in metricConfiguration)
                    //{
                    Metric metricResult = new Metric();
                    var applicationNumber = metricConfiguration.ApplicationNumber.FormatWith(@event);
                    var entityType = metricConfiguration.EntityType.FormatWith(@event);
                    var tenantId = metricConfiguration.TenantId.FormatWith(@event);
                    string originalEvent = metricConfiguration.OriginalEventName.FormatWith(@event);
                    logger.Info($"Processing {@event.Name} for Application Number : #{applicationNumber}");

                    EventMetric configMetric = null;
                    metricConfiguration.EventMetric.TryGetValue(originalEvent, out configMetric);

                    if (configMetric != null)
                    {
                        var productId = "product1";
                        if (!string.IsNullOrWhiteSpace(metricConfiguration.ProductId))
                            productId = metricConfiguration.ProductId.FormatWith(@event);

                        ProductRuleResult ruleResult = await productRuleService.RunRule(entityType, applicationNumber, productId, configMetric.RuleName, @event.Data);
                        if (ruleResult != null && ruleResult.Result == Result.Passed && ruleResult.IntermediateData != null)
                        {
                            var metricData = JObject.FromObject(ruleResult.IntermediateData).ToObject<RuleResponse>();
                            foreach (var metric in configMetric.Metrics)
                            {
                                if (metricData != null)
                                {
                                    BsonDocument resultMetric = new BsonDocument();
                                    resultMetric.Add(new BsonElement("TenantId", tenantId.ToString()));

                                    List<BsonDocument> matchCritiria = new List<BsonDocument>();

                                    var update = Builders<BsonDocument>.Update;
                                    var builder = Builders<BsonDocument>.Filter;
                                    FilterDefinition<BsonDocument> filter = null;

                                    var tenantIdMatch = new BsonDocument { { "$match", new BsonDocument { { "TenantId", tenantId.ToString() } } } };
                                    matchCritiria.Add(tenantIdMatch);
                                    foreach (var dimention in metric.Dimensions)
                                    {
                                        object dim1;
                                        metricData.dimensions.TryGetValue(dimention, out dim1);
                                        var match = new BsonDocument { { "$match", new BsonDocument { { dimention, Convert.ToString(dim1) } } } };
                                        matchCritiria.Add(match);

                                        resultMetric.Add(new BsonElement(dimention, Convert.ToString(dim1)));
                                    }
                                    List<Fact> finalFacts = new List<Fact>();

                                    IMongoDatabase database = metricRepository.GetDatabase();

                                    var collection = database.GetCollection<BsonDocument>(metric.MetricName);

                                    if (collection == null)
                                    {
                                        database.CreateCollection(metric.MetricName);
                                    }
                                    // var pipeline = new[] { matchCritiria };
                                    var existingMetric = collection.Aggregate<BsonDocument>(matchCritiria).ToList();
                                    List<BsonElement> resultFacts = new List<BsonElement>();

                                    foreach (var fact in metric.Facts)
                                    {
                                        object factValue = null;
                                        metricData.facts.TryGetValue(fact.Name, out factValue);

                                        if (factValue != null)
                                        {
                                            double factFinalValue;
                                            double.TryParse(factValue.ToString(), out factFinalValue);
                                            double value = factFinalValue;
                                            if (existingMetric != null && existingMetric.Count > 0)
                                            {
                                                BsonElement existingFact = existingMetric.FirstOrDefault().Elements.Where(x => x.Name == fact.Name).FirstOrDefault();

                                                if (factFinalValue >= 0)
                                                {
                                                    value = (existingFact != null && existingFact.Value != null) ? (double)existingFact.Value : 0;
                                                    if (fact.Operation == OperationType.Increament.ToString())
                                                    {
                                                        value += 1;
                                                    }
                                                    else if (fact.Operation == OperationType.Sum.ToString())
                                                    {
                                                        value += factFinalValue;
                                                    }
                                                }
                                            }
                                            resultFacts.Add(new BsonElement(fact.Name, value));
                                            update.Set(fact.Name, value);
                                        }
                                    }
                                    resultMetric.AddRange(resultFacts);
                                    if (existingMetric == null || existingMetric.Count <= 0)
                                    {
                                        resultMetric.Add(new BsonElement("_id", ObjectId.GenerateNewId()));
                                        collection.InsertOne(resultMetric);
                                    }
                                    else
                                    {
                                        var value = existingMetric.FirstOrDefault().Elements.Where(x => x.Name == "_id").FirstOrDefault().Value;
                                        resultMetric.Add(new BsonElement("_id", value));
                                        filter = builder.Eq("_id", value);
                                        collection.ReplaceOne(filter, resultMetric, new UpdateOptions { IsUpsert = true });
                                        //collection.UpdateOne(filter, resultMetric);
                                    }
                                }
                            }
                        }
                        else
                        {
                            logger.Info($"Rule { configMetric.RuleName} execution failed for Application Number : #{applicationNumber}");
                        }
                    }
                    else
                    {
                        logger.Info($"Configuration not found for  { originalEvent}");
                    }
                }
                catch (Exception ex) { logger.Error($"Unhandled exception while listening event {@event.Name}", ex); }
            };
        }
    }
}


//logger.Info($"New snapshot added to application {applicationNumber}");

//// snapshot creation
//var data = new ApplicationFact();
//data.ApplicationNumber = application.ApplicationNumber;
////data.AnnualIncome = application.in;
//data.ApplicationSource = application.Source.SourceType;
//data.AppliedDateTime = application.ApplicationDate;
//data.City = application.PrimaryAddress.City;
//data.State = application.PrimaryAddress.State;
//data.Zipcode = application.PrimaryAddress.ZipCode;
//data.RequestedTerm = application.RequestedTermValue;
//data.RequestedAmount = application.RequestedAmount;
//data.LoanPurpose = application.PurposeOfLoan;

//var applicationStatus = statusManagementService.GetStatusByEntity("application", applicationNumber);
//if (applicationStatus != null)
//{
//    data.ApplicationStatus = applicationStatus.Name;
//}
//else
//    logger.Info($"No status found for this snapshot");


