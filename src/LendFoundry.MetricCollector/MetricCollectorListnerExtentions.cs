﻿#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
#else
using Microsoft.Framework.DependencyInjection;
using Microsoft.AspNet.Builder;
#endif

namespace LendFoundry.MetricCollector
{
    /// <summary>
    ///  MetricCollectorListnerExtentions class
    /// </summary>
    public static class MetricCollectorListnerExtentions
    {
        /// <summary>
        /// UseMetricCollectorListner
        /// </summary>
        /// <param name="app"></param>
        public static void UseMetricCollectorListner(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IMetricCollectorListner>().Start();
        }
    }
}
