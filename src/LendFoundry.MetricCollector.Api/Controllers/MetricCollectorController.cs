﻿using LendFoundry.Foundation.Services;
using LendFoundry.MetricCollector;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.MetricCollector.Api.Controllers
{
    
    /// <summary>
    /// MetricCollectorController class
    /// </summary>
    [Route("/")]
    public class MetricCollectorController : ExtendedController
    {
        /// <summary>
        /// MetricCollectorController
        /// </summary>
        /// <param name="metricCollectorService"></param>
        public MetricCollectorController(IMetricCollectorService metricCollectorService)
        {
            if (metricCollectorService == null)
                throw new ArgumentNullException(nameof(metricCollectorService));

            MetricCollectorService = metricCollectorService;
        }
        private IMetricCollectorService MetricCollectorService { get; }

        /// <summary>
        ///  GetAllApplicationCountByStatus
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}")]
        public async Task<IActionResult> GetAllApplicationCountByStatus(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await MetricCollectorService.GetAllApplicationCountByStatus(0));
            });
        }

        /// <summary>
        /// GetApplicationCountByTimeLine
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="timePeriod"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{*timePeriod}")]
        public async Task<IActionResult> GetApplicationCountByTimeLine(string entityType,int timePeriod)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await MetricCollectorService.GetAllApplicationCountByStatus(timePeriod));
            });
        }


        /// <summary>
        /// GetAllPartnerApplicationCountByStatus
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="partnerId"></param>
        /// <param name="timePeriod"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/statuscountbypartner/{partnerId}/{*timePeriod}")]
        public async Task<IActionResult> GetAllPartnerApplicationCountByStatus(string entityType,string partnerId,int timePeriod)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await MetricCollectorService.GetAllPartnerApplicationCountByStatus(timePeriod, partnerId));
            });
        }


       /// <summary>
       /// GetPartnerApplicationByTimeLine
       /// </summary>
       /// <param name="entityType"></param>
       /// <param name="partnerId"></param>
       /// <param name="timePeriod"></param>
       /// <returns></returns>
        [HttpGet("{entityType}/partner/{partnerId}/{*timePeriod}")]
        public async Task<IActionResult> GetPartnerApplicationByTimeLine(string entityType, string partnerId, int timePeriod)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await MetricCollectorService.GetPartnerApplicationData(partnerId, timePeriod));
            });
        }
        
        /// <summary>
        /// GetApplicationRejectReasonData
        /// </summary>
        /// <param name="entityType"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/rejectReason")]
        public async Task<IActionResult> GetApplicationRejectReasonData(string entityType)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await MetricCollectorService.GetApplicationRejectReasonData());
            });
        }
       
        /// <summary>
        /// GetAllApplications
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="collectionName"></param>
        /// <param name="fact"></param>
        /// <param name="groupKey"></param>
        /// <param name="operatorData"></param>
        /// <returns></returns>
        [HttpGet("{entityType}/{collectionName}/{fact}/{groupKey}/{operatorData}/metric")]
        public IActionResult GetAllApplications(string entityType, string collectionName, string fact, string groupKey, string operatorData)
        {
            return Execute(() =>
            {
                return Ok(MetricCollectorService.GetData(collectionName, fact, groupKey, operatorData));
            });
        }
    }
}
