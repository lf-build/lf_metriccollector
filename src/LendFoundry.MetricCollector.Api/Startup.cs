﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.MetricCollector.Configurations;
using LendFoundry.MetricCollector.Persistence;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.StatusManagement.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Applications.Filters.Client;
using LendFoundry.MetricCollector;
using System.Threading.Tasks;
using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Persistence.Mongo;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
using Microsoft.AspNet.Http;
#endif

namespace LendFoundry.MetricCollector.Api
{
    internal class Startup
    {

        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Corpository"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.MetricCollector.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddConfigurationService<MetricCollectorConfiguration>(Settings.ServiceName);
            services.AddApplicationsFilterService();
            services.AddStatusManagementService();
            services.AddProductRuleService();
            services.AddMongoConfiguration(Settings.ServiceName);
           services.AddDependencyServiceUriResolver<MetricCollectorConfiguration>(Settings.ServiceName);
            services.AddTransient<MetricCollectorConfiguration>(p => p.GetService<IConfigurationService<MetricCollectorConfiguration>>().Get());
            
            services.AddTransient<IMetricCollectorListner, MetricCollectorListner>();
            //services.AddTransient<IMetricRepository, MetricRepository>();
            services.AddTransient<IMetricBasicRepositoryFactory, MetricRepositoryFactory>();
            services.AddTransient<IMetricCollectorService, MetricCollectorService>();
            services.AddTransient<IMetricBasicRepository, MetricBasicRepository>();
            //services.AddTransient<MetrixCollector.IMongoConfiguration, MetrixCollector.MongoConfiguration>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
            
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "MetricCollector Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
           // app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMetricCollectorListner();
            app.UseMvc();
           
        }
    }
    /// <summary>
    ///  RequestLoggingMiddleware class
    /// </summary>
    public class RequestLoggingMiddleware
    {
        private RequestDelegate Next { get; }

        /// <summary>
        ///  RequestLoggingMiddleware
        /// </summary>
        /// <param name="next"></param>
        public RequestLoggingMiddleware(RequestDelegate next)
        {
            Next = next;
        }

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            //The MVC middleware reads the body stream before we do, which
            //means that the body stream's cursor would be positioned at the end.
            // We need to reset the body stream cursor position to zero in order
            // to read it. As the default body stream implementation does not
            // support seeking, we need to explicitly enable rewinding of the stream:
            try
            {
                await Next.Invoke(context);
            }
            catch (Exception )
            {

            }
        }
    }
}
